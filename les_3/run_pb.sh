ansible-playbook -vv \
	-i inventory/dev/hosts \
	-k -K \
	wordpress.yml "$@"